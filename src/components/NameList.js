import React from 'react';



const NameList = (props)=>{

    const nameList = props.names.map(({ id, name }) => {
        return <li key={id}>{name}</li>;
    });

    return <ul>{nameList}</ul> ;
}


export default NameList;